/**
 *
 * @author SHAON
 */

package testing;

import FileOperation.FileWrite;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.StringTokenizer;
import stemmer.Stemmer;
import preprocessing.*;

public class TestDataProcess 
{
    Integer totalDoc = 0;
    
    public ArrayList<String> token(String input)
    {
        StringTokenizer st = new StringTokenizer(input, " .,_:;?=!“”`~‘’´¡¢£¤§«»¨'\\\"#%*+-/@$&^(){}[]><0123456789",true);  //separates words with this Delimiters except - also returns the delimeter as a string in st
        
        ArrayList<String> tokens = new ArrayList<>();
        
        //System.out.println(input);
        String temp;
        
        boolean neg = false, space = false;
        String negate = "";

        while (st.hasMoreTokens()) 
        {
            //System.out.println(st.nextToken());
            temp = st.nextToken();
            //System.out.println(temp);

            if (temp.equals("not")) {
                neg = true;
                negate = "NOT";

            } else if (temp.equals(" ")) {
                space = true;
                
            } else if (temp.length() < 2 && !Character.isAlphabetic(temp.charAt(0))) {
                neg = false;
                space = true;                                       //considering it as a space and not adding the delimiters into the tokens
                negate = "";
            } else {
                space = false;
                neg = false;
            }

            if (!neg && !space) 
            {
                tokens.add(negate + temp);
            }

        }
        
        return tokens;
    }
    
    public void creatToken(String input, String outFileName) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        input =  new ContractionRmv().removeContractions(input);                //remove contraction & Lowercase
        
        input = new EmoticonsProcess().emotProcess(input);
        
        FileWrite fileOut = new FileWrite();
        fileOut.writeFile(outFileName, false);
        
        ArrayList<String> words = new ArrayList<>();
        
        words.addAll(token(input));

        ArrayList<String> stemmedWords = new Stemmer().stemWords(words);

        LinkedHashSet<String> unqtest = new LinkedHashSet<>(stemmedWords);      //linked hashed set preserves the ordering of data inserted
        
        for (String x : unqtest) {
            fileOut.out.write(x);
            fileOut.out.newLine();
            //System.out.println(x);
        }
        fileOut.fileClose();
        
        words.clear();                      //to clear previous document
        
        totalDoc++;                         //count the number of Documents for Pos/neg

    }
    
}
