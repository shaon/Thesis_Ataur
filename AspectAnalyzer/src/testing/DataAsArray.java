

package testing;

import FileOperation.FileRead;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataAsArray 
{
    HashMap<String, Integer> pos = new HashMap<>();
    HashMap<String, Integer> neg = new HashMap<>();
    
    ArrayList<String> test = new ArrayList<>();
    
    public DataAsArray(String posFile, String negFile, String testFile) throws IOException
    {
        FileRead filePos = new FileRead();
        FileRead fileNeg = new FileRead();
        FileRead fileTest = new FileRead();
        
        filePos.readFile(posFile);
        fileNeg.readFile(negFile);
        fileTest.readFile(testFile);
        
        String input;
        
        while ((input = filePos.in.readLine()) != null) 
        {
            String temp[] = input.split(" : ");

            pos.put(temp[0], Integer.parseInt(temp[1]));    //from file to a Map with (char,freq)
        }

        while ((input = fileNeg.in.readLine()) != null) 
        {
            String temp[] = input.split(" : ");

            neg.put(temp[0], Integer.parseInt(temp[1]));    //from file to a Map with (char,freq)
        }
        
        while((input = fileTest.in.readLine()) != null)
        {
            test.add(input);
        }
        
    }
    
}
