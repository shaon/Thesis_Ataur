/**
 *
 * @author SHAON
 */

package testing;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;


public class Testing 
{
    public CheckIfTrained trainedObj;
    public ClassifierNB positive, negative;
    public Double positivity, negativity;
    

    public Testing(String input) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        trainedObj = new CheckIfTrained();
        
        new TestDataProcess().creatToken(input, "test/test_token.txt");
        
        DataAsArray dataSet = new DataAsArray( "positive/freq_list.txt", "negative/freq_list.txt", "test/test_token.txt");
        
        positive = new ClassifierNB(trainedObj.probPosClass, trainedObj.totalPosWords, trainedObj.vocubularySize, dataSet.pos , dataSet.test);
        negative = new ClassifierNB(trainedObj.probNegClass, trainedObj.totalNegWords, trainedObj.vocubularySize, dataSet.neg , dataSet.test);
        
        
        BigDecimal deno = positive.prob.add(negative.prob);
        

        BigDecimal positiveness = positive.prob.multiply(BigDecimal.valueOf(100));
        
        positiveness = positiveness.divide(deno, 3);                    //round to 3 decimal places
        
        positivity = positiveness.doubleValue();
        
        BigDecimal negativeness = negative.prob.multiply(BigDecimal.valueOf(100));
        
        negativeness = negativeness.divide(deno, 3);
        
        negativity = negativeness.doubleValue();
        
        //positivity = (positive.prob * 100) / (positive.prob + negative.prob) ;
        //negativity = (negative.prob * 100) / (positive.prob + negative.prob) ;
        
    }
    
}
