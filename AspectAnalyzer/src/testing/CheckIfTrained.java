/**
 *
 * @author SHAON
 */
package testing;

import FileOperation.FileRead;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.JOptionPane;
import training.Training;


public class CheckIfTrained 
{
    public Integer totalPosWords, totalNegWords , vocubularySize, totalPosDoc, totalNegDoc;
    
    public Double probPosClass, probNegClass;
    
    public boolean fileInfo() throws IOException
    {
        FileRead train = new FileRead();
        train.readFile("trained.txt");
        
        HashMap<String,String> value = new HashMap<>();
        
        String input;
        Integer tag = 0;
        
        while((input = train.in.readLine()) != null)
        {
             tag= 1;
             
             String temp[] = input.split("=");                                   
             
             try {
                 value.put(temp[0], temp[1]);  
            } 
             catch (ArrayIndexOutOfBoundsException ex) {
                 
                //System.out.println("System is being trained for the first time...");
                JOptionPane.showMessageDialog(null, "System is being trained for the first time...");
                tag = 0;
                break;
            } 
             
        }
        
        if(tag==0)                                                  //no entry in trained file (null)
            return false;
        
        Integer trained = Integer.parseInt(value.get("trained"));
        
        if(trained == 0)                                            //If value of trained is 0 (by user)
        {
            JOptionPane.showMessageDialog(null, "Value of trained file is modified (trained = 0)...\nGetting Trained Again...");
            //System.out.println("Value of trained file is modified (trained = 0)...");
            //System.out.println("Getting Trained Again...");
            return false;
        }
        
        totalPosWords = Integer.parseInt(value.get("totalPosWords"));
        totalNegWords = Integer.parseInt(value.get("totalNegWords"));
        vocubularySize = Integer.parseInt(value.get("vocubularySize"));
        
        probPosClass = Double.parseDouble(value.get("probPosClass"));
        probNegClass = Double.parseDouble(value.get("probNegClass"));
        
        totalPosDoc = Integer.parseInt(value.get("totalPosDoc"));
        totalNegDoc = Integer.parseInt(value.get("totalNegDoc"));
        
        return true;
    }
    
    public CheckIfTrained() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        if(!fileInfo())                                     //check to see whether or not trained
        {
            new Training();
            fileInfo();                                     //call again to fetcht the values..
        }
        
        /*
        System.out.println("Pos Doc = "+totalPosDoc);
        System.out.println("Neg Doc = "+totalNegDoc);
        System.out.println("Pos Words = "+totalPosWords);
        System.out.println("Neg Words = "+totalNegWords);
        */
    }
    
}
