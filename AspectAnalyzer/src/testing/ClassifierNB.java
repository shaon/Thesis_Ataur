/**
 *
 * @author SHAON
 */
package testing;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;


public class ClassifierNB 
{
    public BigDecimal prob = new BigDecimal(1.0);
    
    public ClassifierNB(Double pOfClass, Integer wordsInClass, Integer vocubularySize, HashMap<String, Integer> clas, ArrayList<String> doc)
    {
        Integer freq = 0;
        
        for(String word : doc)
        {
            freq = clas.get(word);                                  //likelihood 
            
            prob = prob.multiply(BigDecimal.valueOf(( (freq == null) ? 1 : freq + 1 )));
            
        }
        
        Double deno = 1/(double)(wordsInClass+vocubularySize);
        
        prob = prob.multiply(BigDecimal.valueOf(pOfClass));
        
        prob = prob.multiply(BigDecimal.valueOf(deno));
        
        //System.out.println(prob);                      
    }
}
