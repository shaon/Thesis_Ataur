package wordtoken;

import FileOperation.*;
import stemmer.*;
import java.io.IOException;
import java.util.*;

public class WordToken {

    public Integer totalDoc = 0;
    
    public ArrayList<String> token(String line)
    {   
        StringTokenizer st = new StringTokenizer(line, " .,_:;?=!“”`~‘’´¡¢£¤§«»¨'\\\"#%*+-/@$&^(){}[]><0123456789");  //separates words with this Delimiters except -
        
        ArrayList<String> tokens = new ArrayList<>();
        
        while(st.hasMoreTokens())
        {
            //System.out.println(st.nextToken());
            tokens.add(st.nextToken());
        }
         
        return tokens;
    }
    
    public void creatToken(String inpFileName, String outFileName) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        FileRead fileIn = new FileRead();
        FileWrite fileOut = new FileWrite();
        
        ArrayList<String> words = new ArrayList<>();
        //ArrayList<String> stemmedWords = new ArrayList<>();
        
        fileIn.readFile(inpFileName);
        fileOut.writeFile(outFileName, false);
        
        String input;
        
        while((input=fileIn.in.readLine())!=null)             //untile the end of file
        {
            if(!input.equals("#"))                          //until a document
            {
                words.addAll(token(input));
            }
            else
            {
                ArrayList<String> stemmedWords =  new Stemmer().stemWords(words);
                
                for(String x : stemmedWords)
                {
                    fileOut.out.write(x);
                    fileOut.out.newLine();
                    //System.out.println(x);
                }
                
                fileOut.out.write("#");
                fileOut.out.newLine();
                //System.out.println("#");
                
                words.clear();                      //to clear previous document
                //break;
                totalDoc++;                         //count the number of Documents for Pos/neg
            }
        }
        
        fileOut.fileClose();
    }
    
}
