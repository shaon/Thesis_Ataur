/**
 *
 * @author SHAON
 */

package preprocessing;

import FileOperation.FileRead;
import FileOperation.FileWrite;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;


public class Negation 
{
    public HashMap<String, Integer> stopwords = new HashMap<>();
    
    public Negation(String inFileName, String outFileName) throws IOException
    {
        
        FileRead fileIn = new FileRead();
        FileWrite fileOut = new FileWrite();
        
        
        fileIn.readFile(inFileName);
        fileOut.writeFile(outFileName, false);
        
        String input,temp;
       
        while((input=fileIn.in.readLine())!=null)             //untile the end of file
        {
            StringTokenizer st = new StringTokenizer(input, " .,_:;?=!“”`~‘’´¡¢£¤§«»¨'\\\"#%*+-/@$&^(){}[]><0123456789",true);  //separates words with this Delimiters also returns the delimeter as a string in st

            
            String output="";
            
            boolean neg = false, space = false;
            String negate = "";
            
            while (st.hasMoreTokens()) 
            {
                temp = st.nextToken();
                
                if(temp.equals("not"))
                {
                    neg=true;
                    negate = "NOT";
               
                }
                else if(temp.equals(" "))
                {
                    space = true;
                    output+= " ";
                }
                else if(temp.length()<2 && !Character.isAlphabetic(temp.charAt(0)))
                {
                    neg = false;
                    space = false;
                    negate = "";
                }
                else
                {
                    space = false;
                    neg = false;
                }
                    
                
                if (!neg && !space) 
                {
                    output += (negate + temp);
                }
                 
            }
            
            //System.out.println(output+"\n");
            fileOut.out.write(output);
            fileOut.out.newLine();
           
        }
        
        fileOut.fileClose();
    }
    
}
