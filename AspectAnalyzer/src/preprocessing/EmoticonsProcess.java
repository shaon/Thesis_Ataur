/**
 *
 * @author SHAON
 */

package preprocessing;

import FileOperation.FileRead;
import FileOperation.FileWrite;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;


public class EmoticonsProcess 
{
    public HashMap<String,String> emoticon = new HashMap<>();
    
    public String emotProcess(String inText) throws IOException                       //for Testing***
    {
        FileRead emoFile = new FileRead();
        emoFile.readFile("emoticons.txt");
        
        String input;
        
        while((input=emoFile.in.readLine())!=null)             //untile the end of file
        {
            StringTokenizer st = new StringTokenizer(input, " ");
            
            emoticon.put(st.nextToken(), st.nextToken());
        }
        
        for(String x : emoticon.keySet())
        {
            //System.out.println(x+"      "+emoticon.get(x));             
            inText = inText.replace(x, " EMO"+emoticon.get(x)+" ");        // Adding a delimeter so that it can't effect negation
        }
        
        return inText;
    }
    
    public EmoticonsProcess()
    {
        //just to use the method removeContraction <Is an Illusion> ^_^
    }
            
    public EmoticonsProcess(String inFileName, String outFileName) throws IOException        //for Training**
    {
        FileRead emoFile = new FileRead();
        FileRead fileIn = new FileRead();
        FileWrite fileOut = new FileWrite();
        
        emoFile.readFile("emoticons.txt");
        fileIn.readFile(inFileName);
        fileOut.writeFile(outFileName, false);
        
        String input;
        
        
        while((input=emoFile.in.readLine())!=null)             //untile the end of file
        {
            StringTokenizer st = new StringTokenizer(input, " ");
            
            emoticon.put(st.nextToken(), st.nextToken());
        
        }
        
        while((input=fileIn.in.readLine())!=null)             //untile the end of file
        {
            
            for(String x : emoticon.keySet())
            {
                //System.out.println(x+"      "+emoticon.get(x));             
                input = input.replace(x, " EMO"+emoticon.get(x)+" ");

            }
            
            fileOut.out.write(input);
            fileOut.out.newLine();
               
        }
        
        fileOut.fileClose();
    }
    
}
