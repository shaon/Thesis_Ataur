/**
 *
 * @author SHAON
 */

package preprocessing;

import FileOperation.FileRead;
import FileOperation.FileWrite;
import java.io.IOException;

public class ContractionRmv 
{
    public String removeContractions(String inputString) 
    {
        inputString = inputString.toLowerCase();
        
        inputString = inputString.replaceAll("<br />", "");                         //HTML Tag remove
        inputString = inputString.replaceAll("cannot", "can not");
        inputString = inputString.replaceAll("can't", "can not");                  //Abnormal Cases... ***
        inputString = inputString.replaceAll("won't", "will not");
        inputString = inputString.replaceAll("ain't", "am not");
        
        inputString = inputString.replaceAll("n't", " not");  
        inputString = inputString.replaceAll("'re", " are");
        inputString = inputString.replaceAll("'m", " am");
        inputString = inputString.replaceAll("'ll", " will");
        inputString = inputString.replaceAll("'ve", " have");
        inputString = inputString.replaceAll("'d", " would");
        inputString = inputString.replaceAll("'s", " is");

        return inputString;
    }
    
    public ContractionRmv()
    {
        //just to use the method removeContraction <Is an Illusion> ^_^  
    }
    
    public ContractionRmv(String inFileName, String outFileName) throws IOException
    {
        
        FileRead fileIn = new FileRead();
        FileWrite fileOut = new FileWrite();
        
        fileIn.readFile(inFileName);
        fileOut.writeFile(outFileName, false);
        
        String input;
        
        while((input=fileIn.in.readLine())!=null)             //untile the end of file
        {
            if(!input.equals("#"))                          //until a document
            {
                
                input = removeContractions(input);
                
                fileOut.out.write(input);
                fileOut.out.newLine();
            
            }
            else
            {
                fileOut.out.write("#");
                fileOut.out.newLine();
               
            }
        }
        
        fileOut.fileClose();
    }
}
