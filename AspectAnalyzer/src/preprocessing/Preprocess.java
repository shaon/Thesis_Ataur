/**
 *
 * @author SHAON
 */
package preprocessing;

import java.io.IOException;


public class Preprocess 
{
    public Preprocess(String inPath, String outPath) throws IOException
    {
        new ContractionRmv(inPath+"input.txt", outPath+"contraction_removed.txt");
        new EmoticonsProcess(inPath+"contraction_removed.txt", outPath+"emot_processed.txt");
        
        new StopWord_Negation(inPath+"emot_processed.txt", outPath+"stop_neg.txt");
        //new Negation(inPath+"emot_processed.txt", outPath+"stop_neg.txt");                  //use either one of these classses
    }
    
}
