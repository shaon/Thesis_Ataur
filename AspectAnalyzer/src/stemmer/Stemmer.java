package stemmer;

import java.io.IOException;
import java.util.ArrayList;

public class Stemmer {
    
   public ArrayList<String> stemWords(ArrayList<String> words) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException
   {
       
       ArrayList<String> steamWords = new ArrayList<>();
       
       Class stemClass = Class.forName("stemmer.ext." + "english" + "Stemmer");
       SnowballStemmer stemmer = (SnowballStemmer) stemClass.newInstance();

       for(String x : words) 
       {
           stemmer.setCurrent(x);
           stemmer.stem();
           steamWords.add(stemmer.getCurrent());      
       }  
       
       return steamWords;
   }

}
