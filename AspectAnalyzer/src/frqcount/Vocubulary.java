/**
 *
 * @author SHAON
 */
package frqcount;

import FileOperation.FileRead;
import FileOperation.FileWrite;
import java.io.IOException;
import java.util.TreeSet;

public class Vocubulary 
{
    public Integer vocubularySize;
    
    public Vocubulary(String posFile, String negFile, String outFile) throws IOException
    {
        FileRead filePos = new FileRead();
        FileRead fileNeg = new FileRead();
        FileWrite fileVoc = new FileWrite();
        
        filePos.readFile(posFile);
        fileNeg.readFile(negFile);
        fileVoc.writeFile(outFile, false);
        
        TreeSet<String> voc = new TreeSet<>();           //to store the unique words/vocubulary from each class
        
        String input;
        
        while((input = filePos.in.readLine()) != null)
        {
            voc.add(input);
        }
        
        while((input = fileNeg.in.readLine()) != null)
        {
            voc.add(input);
        }
        
        vocubularySize = voc.size();
        
        for(String x : voc)                            //***Creation of Vovubulary is Optional
        {
            fileVoc.out.write(x);
            fileVoc.out.newLine();
        }
        fileVoc.fileClose();
    }
    
}
