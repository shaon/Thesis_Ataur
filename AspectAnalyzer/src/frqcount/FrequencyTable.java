/**
 *
 * @author SHAON
 */
package frqcount;

import FileOperation.FileRead;
import FileOperation.FileWrite;
import java.io.IOException;
import java.util.HashMap;


public class FrequencyTable 
{
    public Integer totalCount;
    
    public void creatTable(String inpFileName, String outFileName) throws IOException
    {
        FileRead fileIn = new FileRead();
        FileWrite fileOut = new FileWrite();
        
        fileIn.readFile(inpFileName);
        fileOut.writeFile(outFileName, false);
        
        
        HashMap<String, Integer> words = new HashMap<>();
        
        String token;
        Integer preVal=0;
        
        while((token = fileIn.in.readLine()) != null)
        {
            preVal = words.get(token); 
            words.put(token, (preVal==null) ? 1 : preVal+1);
        }
        
        totalCount = preVal = 0;
                
        for(String key : words.keySet())
        {
             preVal = words.get(key);
             totalCount += preVal;                    //total number of words = size of mega_doc
             
             fileOut.out.write(key +" : "+ preVal);
             fileOut.out.newLine();
        }
        
        fileOut.fileClose();
    }
}
