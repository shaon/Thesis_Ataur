/**
 *
 * @author SHAON
 */

package frqcount;

import FileOperation.*;
import java.io.IOException;
import java.util.ArrayList;


public class MegaDoc 
{

    public void megaDoc(String inpFileName, String outFileName) throws IOException
    {
        FileRead fileIn = new FileRead();
        FileWrite fileOut = new FileWrite();
        
        ArrayList<String> words = new ArrayList<>();
        
        fileIn.readFile(inpFileName);
        fileOut.writeFile(outFileName, false);
        
        String token;
        
        while((token = fileIn.in.readLine()) != null)
        {
            if(!token.equals("#"))
            {
                //System.out.println(token);
                words.add(token);
            }
            else
            {
                ArrayList<String> unique = new UniqueWord().removeDuplicate(words);
                
                for(String x : unique)
                {
                    //System.out.println(x);
                    fileOut.out.write(x);
                    fileOut.out.newLine();
                }
                
                words.clear();
            }
        }
        fileOut.fileClose();
    }
   
}
