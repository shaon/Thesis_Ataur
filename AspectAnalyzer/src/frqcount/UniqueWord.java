/**
 *
 * @author SHAON
 */

package frqcount;

import java.util.ArrayList;
import java.util.TreeSet;


public class UniqueWord 
{
    public ArrayList<String> removeDuplicate(ArrayList<String> doc)
    {
        TreeSet<String> words = new TreeSet<>(doc);
        
        return new ArrayList<>(words);
          
    }
}
