package training;

import frqcount.FrequencyTable;
import frqcount.MegaDoc;
import java.io.IOException;
import wordtoken.WordToken;


public class PosNegList {
    
    Integer totalWordCount, totalDoc;
    
    public PosNegList(String path) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        WordToken tokenized = new WordToken();                                  //tokenization + stemming
        tokenized.creatToken(path+"stop_neg.txt" , path+"tokn_stem_out.txt");
        totalDoc = tokenized.totalDoc;
        
        new MegaDoc().megaDoc(path+"tokn_stem_out.txt" , path+"mega_doc.txt");      // megaDoc creation
        
        FrequencyTable freq = new FrequencyTable();
        freq.creatTable(path+"mega_doc.txt" , path+"freq_list.txt");
        totalWordCount = freq.totalCount;
        
    }
    
}
