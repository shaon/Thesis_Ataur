/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package training;

import FileOperation.FileWrite;
import frqcount.Vocubulary;
import java.io.IOException;
import preprocessing.*;
/**
 *
 * @author SHAON
 */
public class Training 
{
    Integer totalPosWords, totalNegWords , vocubularySize, totalPosDoc, totalNegDoc;
    
    Double probPosClass, probNegClass;
            
    public void saveFile() throws IOException
    {
        FileWrite train = new FileWrite();
        train.writeFile("trained.txt", false);
                                               //saving the info for testing phase not to run training phase again and again
        train.out.write("trained=1");
        train.out.newLine();
        train.out.write("totalPosWords="+totalPosWords);
        train.out.newLine();
        train.out.write("totalNegWords="+totalNegWords);
        train.out.newLine();
        train.out.write("vocubularySize="+vocubularySize);
        train.out.newLine();
        train.out.write("probPosClass="+probPosClass);
        train.out.newLine();
        train.out.write("probNegClass="+probNegClass);
        train.out.newLine();
        train.out.write("totalPosDoc="+totalPosDoc);
        train.out.newLine();
        train.out.write("totalNegDoc="+totalNegDoc);
        train.out.newLine();
        
        train.fileClose();
    }
    
    public Training() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        new Preprocess("positive/", "positive/");              //second portion is just file path ***
        new Preprocess("negative/", "negative/");
        
        PosNegList pos = new PosNegList("positive/");           
        PosNegList neg = new PosNegList("negative/");
        
        Vocubulary voc = new Vocubulary("positive/mega_doc.txt", "negative/mega_doc.txt", "vocubulary.txt");
        
        totalPosWords = pos.totalWordCount;
        totalNegWords = neg.totalWordCount;
        vocubularySize = voc.vocubularySize;
        
        totalPosDoc = pos.totalDoc;
        totalNegDoc = neg.totalDoc;
        
        probPosClass = (totalPosDoc / (double)(totalPosDoc+totalNegDoc));
        probNegClass = (totalNegDoc / (double)(totalPosDoc+totalNegDoc));
        
        saveFile();
    }
    
}
