package FileOperation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import javax.swing.JOptionPane;

/**
 *
 * @author SHAON
 */
public class FileWrite 
{
    public BufferedWriter out=null;
    
    public void writeFile(String filename, boolean append)
    {
        try{
            out = new BufferedWriter(new FileWriter("src/Files/" + filename, append)); 
            
            /**************For Jar File do this instead of above line******************/
            /*
            Class cls = FileRead.class;
            ProtectionDomain domain = cls.getProtectionDomain();
            CodeSource source = domain.getCodeSource();
            URL url = source.getLocation();
            String relPath = url.getPath();
            
            int i = relPath.lastIndexOf("/");
   
            String path = relPath.substring(0, i);
            
            out = new BufferedWriter(new FileWriter(path+"/Files/" + filename, append)); 
            */
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "I/O error In WriteFile Class.\n"+ex.getMessage());
        }
    }
    
    public void fileClose() throws IOException
    {
        out.close();
    }
}
