/**
 *
 * @author SHAON
 */
package FileOperation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import javax.swing.JOptionPane;


public class FileRead 
{
    public BufferedReader in=null;
    
    
    public void readFile(String filename)
    {
        try {
            in = new BufferedReader(new FileReader("src/Files/" + filename));
            
            /**************For Jar File do this instead of above line******************/
            /*
            Class cls = FileRead.class;
            ProtectionDomain domain = cls.getProtectionDomain();
            CodeSource source = domain.getCodeSource();
            URL url = source.getLocation();
            String relPath = url.getPath();
            
            int i = relPath.lastIndexOf("/");
            
            String path = relPath.substring(0, i);
            
            in = new BufferedReader(new FileReader(path+"/Files/" + filename));
         */
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"File not found in readFile method.\n"+ex.getMessage());
        }
    }
    
    public void fileClose() throws IOException
    {
        in.close();
    }
}

